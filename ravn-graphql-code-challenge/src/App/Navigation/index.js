import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import * as routes from '../../constants/routes';
import Button from '../../Button';
import Input from '../../Input';

import './style.css';

const Navigation = ({
  location: { pathname },
  RepositoryName,
  onRepositorySearch,
}) => (
  <header className="Navigation">

    <div className="Navigation-link">
      <Link to={routes.REPOSITORY}>Github's Reposities</Link>
    </div>

  <div className="Navigation-link">
      <Link to={routes.USER}>Github's USers</Link>
    </div>

    {pathname === routes.REPOSITORY && (
      <RepositorySearch
        RepositoryName={RepositoryName}
        onRepositorySearch={onRepositorySearch}
      />
    )}
  </header>
);

class RepositorySearch extends React.Component {
  state = {
    value: this.props.RepositoryName,
  };

  onChange = event => {
    this.setState({ value: event.target.value });
  };

  onSubmit = event => {
    this.props.onRepositorySearch(this.state.value);

    event.preventDefault();
  };

  render() {
    const { value } = this.state;

    return (
      <div className="Navigation-search">
        <form onSubmit={this.onSubmit}>
          <Input
            color={'white'}
            type="text"
            value={value}
            onChange={this.onChange}
          />{' '}
          <Button color={'white'} type="submit">
            Search
          </Button>
        </form>
      </div>
    );
  }
}

export default withRouter(Navigation);

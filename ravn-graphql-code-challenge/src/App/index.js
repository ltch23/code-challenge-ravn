import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Navigation from './Navigation';
import Repository from '../Repository';
import User from '../User';
import * as routes from '../constants/routes';


class App extends Component {
  state = {
    repositoryName: 'graphql',
  };
  onRepositorySearch = value => {
    this.setState({ repositoryName: value });
 };

  render() {
    const { repositoryName } = this.state;

    return  (
    <Router>
    <div className="App">
      <Navigation
        repositoryName={repositoryName}
        onRepositorySearch={this.onRepositorySearch}
      />


      <div className="App-main">
        <Route
          exact
          path={routes.REPOSITORY}
          component={() => (
            <div className="App-content_large-header">
              <Repository repositoryName={repositoryName} />
            </div>
          )}
        />
         <Route
              exact
              path={routes.USER}
              component={() => (
                <div className="App-content_small-header">
                  <User />
                </div> 
              )}
            />
        </div>
    </div>
  </Router>
);


  }
}


export default App;

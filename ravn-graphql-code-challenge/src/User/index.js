import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Link from '../Link'


const GET_USUARIOS = gql`{
   repository(name: "is2-inventarios", owner: "jonmoll") {
    collaborators(affiliation: ALL, ) {
    totalCount
      nodes{
        name
        login
        contributionsCollection{
          totalCommitContributions
        }
      }
    }
  }
}

` 
  
 const User = () => (

    <Query
    query={GET_USUARIOS}
        > 
    {result =>{
      return(
   <ul>
    {result.loading ? '' : result.data.repository.collaborators.nodes.map((row, i) =>
      <li >
      <div className="RepositoryItem-title">
        <h2>
          <Link href={row.name}>{row.login}</Link>
        </h2>      
      </div>
      
       <div>
        total de comits: {row.contributionsCollection.totalCommitContributions}
      </div> 

      </li>
    )}
  </ul>
      );
  } }  
   </Query>
 
);
 

export default User

import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import Link from '../Link'


const POPULAR_REPOSITORIES_LIST = gql`
  query($repositoryName: String!){
  search(query: $repositoryName, type: REPOSITORY, first: 10) {
    repositoryCount
    edges {
      node {
        ... on Repository {
          name
          owner {
            login
          }
          stargazers {
            totalCount
          }
          url
          descriptionHTML
          primaryLanguage{
            name
          }
          
        }
      }
    }
  }
}

` 

  
  const Repository = ({ repositoryName }) => (

    <Query
    query={POPULAR_REPOSITORIES_LIST}
    variables={{
      repositoryName,
    }}> 
    {result =>{
      return(
  <ul>
    {result.loading ? '' : result.data.search.edges.map((row, i) =>
      <li >
      <div className="RepositoryItem-title">
        <h2>
          <Link href={row.node.url}>{row.node.name}</Link>
        </h2>      
      </div>

      <div className="RepositoryItem-description">
      <div
        className="RepositoryItem-description-info"
        dangerouslySetInnerHTML={{ __html: row.node.descriptionHTML }}
      />
      <div className="RepositoryItem-description-details">
        <div>
          {row.node.primaryLanguage && (
            <span>Language: {row.node.primaryLanguage.name}</span>
          )}
        </div>
        <div>
          {row.node.owner && (
            <span>
              Owner: <a href={row.node.owner.url}>{row.node.owner.login}</a>
            </span>
          )}
        </div>
        </div>
        </div>




      </li>
    )}
  </ul>
      );
  } }  
   </Query>
 
);
 



export default Repository

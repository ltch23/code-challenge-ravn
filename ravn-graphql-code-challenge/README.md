# A React + Apollo + GraphQL GitHub Client
## Caracteristicas

* React 16 con  create-react-app
* Responsive
* React Router 4
* Apollo con GitHub GraphQL API

## Instalación
`git clone https://gitlab.com/ltch23/code-challenge-ravn`| 
* cd react-graphql-github-apollo
* agregar su propio [personal acceso de token GitHub  ](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/) en un archivo .env  en tu raiz del folder
  * permisos que se requieren: admin:org, repo, user, notifications
  * REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN=xxxXXX
* npm install
* npm start
* visit `http://localhost:3000`

## Fuentes de  React + GraphQL + Apollo

* [React Courses](https://roadtoreact.com)
* [A React + Apollo + GraphQL GitHub Client](https://github.com/the-road-to-graphql/react-graphql-github-apollo)
